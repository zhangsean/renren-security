package io.renren.controller;

import com.alibaba.fastjson.JSON;
import io.renren.service.SysGeneratorService;
import io.renren.utils.PageUtils;
import io.renren.utils.Query;
import io.renren.utils.R;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 代码生成器
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年12月19日 下午9:12:58
 */
@Controller
@RequestMapping("/sys/generator")
public class SysGeneratorController {
	@Autowired
	private SysGeneratorService sysGeneratorService;
	
	/**
	 * 列表
	 */
	@ResponseBody
	@RequestMapping("/list")
	public R list(@RequestParam Map<String, Object> params){
		//查询列表数据
		Query query = new Query(params);
		List<Map<String, Object>> list = sysGeneratorService.queryList(query);
		int total = sysGeneratorService.queryTotal(query);
		
		PageUtils pageUtil = new PageUtils(list, total, query.getLimit(), query.getPage());
		
		return R.ok().put("page", pageUtil);
	}

	/**
	 * 生成代码
	 */
	@RequestMapping("/code")
	public void code(String tables, HttpServletRequest request, HttpServletResponse response) throws IOException{
		String[] tableNames = new String[]{};
		tableNames = JSON.parseArray(tables).toArray(tableNames);

		if (request.getMethod().equals("GET")) {
            byte[] data = sysGeneratorService.generatorCode(tableNames, true);

            response.reset();
            response.setHeader("Content-Disposition", "attachment; filename=\"renren.zip\"");
            response.addHeader("Content-Length", "" + data.length);
            response.setContentType("application/octet-stream; charset=UTF-8");

            IOUtils.write(data, response.getOutputStream());
        } else if (request.getMethod().equals("POST")) {
		    request.getSession().getServletContext().getRealPath("/");
            sysGeneratorService.generatorCode(tableNames, false);

            byte[] msg = "{\"code\": 0, \"msg\": \"生成代码成功，请刷新项目查看\"}".getBytes();
            response.reset();
            response.addHeader("Content-Length", "" + msg.length);
            response.setContentType("application/json; charset=UTF-8");
            response.getOutputStream().write(msg);
        }
	}
}
